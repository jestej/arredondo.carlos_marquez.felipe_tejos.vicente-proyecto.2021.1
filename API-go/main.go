package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type product struct {
	ID     int    `json:ID`
	Name   string `json:Name`
	Units  int    `json:Units`
	Price  int    `json:Price`
	Market string `json:Market`
}

type allProducts {}product

var products = allProducts{
	//Lider
	{
		ID:     1,
		Name:   "Lechuga Unidad",
		Units:  10,
		Price:  850,
		Market: "Supermercado 1",
	},
	{
		ID:     2,
		Name:   "Papas Malla 2Kg",
		Units:  5,
		Price:  2250,
		Market: "Supermercado 1",
	},
	{
		ID:     3,
		Name:   "Zanahorias 1Kg",
		Units:  7,
		Price:  850,
		Market: "Supermercado 1",
	},
	//Jumbo
	{
		ID:     4,
		Name:   "Lechuga Unidad",
		Units:  15,
		Price:  790,
		Market: "Supermercado 2",
	},
	{
		ID:     5,
		Name:   "Papas 1kg",
		Units:  7,
		Price:  1290,
		Market: "Supermercado 2",
	},
	{
		ID:     6,
		Name:   "Zanahorias 1Kg",
		Units:  7,
		Price:  890,
		Market: "Supermercado 2",
	},
}

func getProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-type", "application/json")
	json.NewEncoder(w).Encode(products)
}

func createProduct(w http.ResponseWriter, r *http.Request) {
	var newProduct product
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Inserta un producto valido")
	}

	json.Unmarshal(reqBody, &newProduct)

	newProduct.ID = len(products) + 1
	products = append(products, newProduct)

	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newProduct)
}

func getProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	productID, err := strconv.Atoi(vars["id"])
	if err != nil {
		fmt.Fprintf(w, "ID Invalido")
		return
	}

	for _, product := range products {
		if product.ID == productID {
			w.Header().Set("Content-type", "application/json")
			json.NewEncoder(w).Encode(product)
		}
	}
}

func deleteProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	productID, err := strconv.Atoi(vars["id"])
	if err != nil {
		fmt.Fprintf(w, "ID Invalido")
		return
	}

	for i, product := range products {
		if product.ID == productID {
			products = append(products[:i], products[i+1:]...)
			fmt.Fprintf(w, "El producto con el ID %v ha sido removido satisfactoriamente", productID)
		}
	}
}

func updateProduct(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	productID, err := strconv.Atoi(vars["id"])
	var updatedProduct product
	if err != nil {
		fmt.Fprintf(w, "ID Invalido")
		return
	}
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Inserte Datos Validos")
	}
	json.Unmarshal(reqBody, &updatedProduct)
	for i, product := range products {
		if product.ID == productID {
			products = append(products[:i], products[i+1:]...)
			updatedProduct.ID = productID
			products = append(products, updatedProduct)
			fmt.Fprintf(w, "El producto con el ID %v ha sido actualizado satisfactoriamente", productID)
		}
	}
}

//--------------------------- Funciones Markets --------------------//
func getMarkets(w http.ResponseWriter, r *http.Request) {

	var markets []string
	for _, market := range products {
		if !existeEnArreglo(markets, market.Market) {
			markets = append(markets, market.Market)
		}
	}
	w.Header().Set("Content-type", "application/json")
	json.NewEncoder(w).Encode(markets)

}
func existeEnArreglo(arreglo []string, busqueda string) bool {
	for _, dato := range arreglo {
		if dato == busqueda {
			return true
		}
	}
	return false
}

func getProductsByMarket(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	market_find := vars["market"]

	var markets []string
	for _, market_product := range products {
		if !existeEnArreglo(markets, market_product.Market) {
			markets = append(markets, market_product.Market)
		}
	}
	if !(existeEnArreglo(markets, market_find)) {
		fmt.Fprintf(w, "El supermercado %v No existe", market_find)
		return
	}
	var productsByMarket = allProducts{}
	for _, product := range products {
		if product.Market == market_find {
			productsByMarket = append(productsByMarket, product)
		}
	}
	json.NewEncoder(w).Encode(productsByMarket)
}

//----------------------- Fin Market ---------------------------//

//----------------------- Index APi ----------------------------//
func IndexRoute(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Bienvedo a la Api para el proyecto de Elo-329 IntelliEat")
}

//----------------------- Fin Index Api ---------------------------//
func main() {
	port := flag.Int("port", 8181, "Ingrese Puerto")
	flag.Parse()
	if *port == 8181 {
		flag.PrintDefaults()
	}
	router := mux.NewRouter().StrictSlash(true)

	//---------------------Inicio de la API---------------------------//
	router.HandleFunc("/", IndexRoute)

	//-------------Muestra todos los productos cargados---------------//
	router.HandleFunc("/products", getProducts).Methods("GET")

	//----------Permite crear un producto a traves de POST------------//
	router.HandleFunc("/products", createProduct).Methods("POST")

	//----------Permite Obtener un producto a travez de su id----------//
	router.HandleFunc("/products/{id}", getProduct).Methods("GET")

	//----------Permite eliminar un producto a traves de su ID---------//
	router.HandleFunc("/products/{id}", deleteProduct).Methods("DELETE")

	//----------Permite editar un producto a traves de su ID-----------//
	router.HandleFunc("/products/{id}", updateProduct).Methods("PUT")

	//----------Muestra todos los mercados disponibles en una Lista ---//
	router.HandleFunc("/markets", getMarkets).Methods("GET")

	//----Muestra todos los productos de un supermercado pasado por parametro -------//
	router.HandleFunc("/{market}/products", getProductsByMarket).Methods("GET")

	log.Printf("Servidor inicializandose en localhost:" + strconv.Itoa(*port))
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(*port), router))

}
