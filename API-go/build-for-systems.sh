#!/bin/bash

# Windows
# 64 - bit
GOOS=windows GOARCH=amd64 go build -o bin/api-64-bits.exe main.go
# 32 - bit
GOOS=windows GOARCH=386 go build -o bin/api-32-bits.exe main.go

# Mac OS
# 64-bit
GOOS=darwin GOARCH=amd64 go build -o bin/app-amd64-macos main.go

# 32-bit
GOOS=darwin GOARCH=386 go build -o bin/api-386-macos main.go

# Linux
# 64-bit
GOOS=linux GOARCH=amd64 go build -o bin/api-amd64-linux main.go

# 32-bit
GOOS=linux GOARCH=386 go build -o bin/api-386-linux main.go
