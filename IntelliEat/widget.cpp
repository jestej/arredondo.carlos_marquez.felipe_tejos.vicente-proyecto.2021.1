#include "widget.h"
#include "ui_widget.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QtNetwork/QNetworkAccessManager>
#include <QUrlQuery>
#include <string>
#include <iostream>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    get_TitleSupermarket("http://200.104.181.120:5000/api/mercados");


}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_add_clicked()
{
    QNetworkAccessManager* nam = new QNetworkAccessManager(this);
        connect(nam, &QNetworkAccessManager::finished, this, &Widget::chargerSupermarket);
        QUrl url("http://200.104.181.120:5000/api/productos");
        qDebug()<< "url: "<< url.toString(QUrl::FullyEncoded);
        nam->get(QNetworkRequest(url));
}


void Widget::on_delete_2_clicked()
{
    QString deleteProducto=ui->lineEdit->text().toLower();
    for(int row = 0; row < ui->sDesear->count(); row++)
    {
             QListWidgetItem *item = ui->sDesear->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                ui->sDesear->removeItemWidget(item);
                delete item; // Qt documentation warnings you to destroy item to effectively remove it from QListWidget.
             }

    }
    for(int row = 0; row < ui->s1->count(); row++)
    {
             QListWidgetItem *item = ui->s1->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                s1PriceTotal-=(item->text().split(" ")[item->text().split(" ").length()-1].toInt());
                ui->s1->removeItemWidget(item);
                ui->s1P->setText("Precio Total: "+ QString::number(s1PriceTotal));
                delete item; // Qt documentation warnings you to destroy item to effectively remove it from QListWidget.
             }

    }
    for(int row = 0; row < ui->s2->count(); row++)
    {
             QListWidgetItem *item = ui->s2->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                s2PriceTotal-=(item->text().split(" ")[item->text().split(" ").length()-1].toInt());
                ui->s2->removeItemWidget(item);
                ui->s2P->setText("Precio Total: "+ QString::number(s2PriceTotal));
                ui->s2->removeItemWidget(item);
                delete item; // Qt documentation warnings you to destroy item to effectively remove it from QListWidget.
             }

    }
    for(int row = 0; row < ui->s3->count(); row++)
    {
             QListWidgetItem *item = ui->s3->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                s3PriceTotal-=(item->text().split(" ")[item->text().split(" ").length()-1].toInt());
                ui->s3->removeItemWidget(item);
                ui->s3P->setText("Precio Total: "+ QString::number(s3PriceTotal));
                ui->s3->removeItemWidget(item);
                delete item; // Qt documentation warnings you to destroy item to effectively remove it from QListWidget.
             }
             // process item

    }
    ui->lineEdit->setText("");
}
void Widget::chargerSupermarket(QNetworkReply *reply){


    if(reply->error() == QNetworkReply::NoError){

        QByteArray result = reply->readAll();
        qDebug() << result;
        QJsonDocument jsonResponse = QJsonDocument::fromJson(result);

        QJsonObject obj = jsonResponse.object();
        QJsonArray array = obj["productos"].toArray();

        for(const QJsonValue & value : array) {
        if ((value["nombrep"].toString().toLower()+" "+value["detallep"].toString().toLower()).contains(ui->lineEdit->text().toLower())){
                ui->sDesear->addItem(value["nombrep"].toString()+ " "+value["detallep"].toString() );
                QString objeto = value["nombrep"].toString()+ " "+value["detallep"].toString() + " " +value["marcap"].toString()+" " +QString::number(value["price"].toInt());
                if (value["super_id"].toInt()==1){
                    ui->s1->addItem(objeto);
                    s1PriceTotal+=value["price"].toInt();
                    ui->s1P->setText("Precio Total: "+ QString::number(s1PriceTotal));
                    ui->s1P->setFont(QFont( "Arial", 10, QFont::Bold));
                }
                if (value["super_id"].toInt()==2){
                    ui->s2->addItem(objeto);
                    s2PriceTotal+=value["price"].toInt();
                    ui->s2P->setText("Precio Total: "+ QString::number(s2PriceTotal));
                    ui->s2P->setFont(QFont( "Arial", 10, QFont::Bold));
                }
                if (value["super_id"].toInt()==3){
                    ui->s3->addItem(objeto);
                    s3PriceTotal+=(value["price"].toInt());
                    qDebug()<<"Precio Total: "+ QString::number(s3PriceTotal);
                    ui->s3P->setText("Precio Total: "+ QString::number(s3PriceTotal));
                    ui->s3P->setFont(QFont( "Arial", 10, QFont::Bold));
                }
        }
        }
    }
    else
        qDebug() << "ERROR";
    reply->deleteLater();
}


void Widget::on_sDesear_itemDoubleClicked(QListWidgetItem *item)
{
    QString deleteProducto=item->text().toLower();
    for(int row = 0; row < ui->sDesear->count(); row++)
    {
             QListWidgetItem *item = ui->sDesear->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                ui->sDesear->removeItemWidget(item);
                delete item; // Qt documentation warnings you to destroy item to effectively remove it from QListWidget.
             }
             // process item

    }
    for(int row = 0; row < ui->s1->count(); row++)
    {
             QListWidgetItem *item = ui->s1->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                s1PriceTotal-=(item->text().split(" ")[item->text().split(" ").length()-1].toInt());
                ui->s1->removeItemWidget(item);
                ui->s1P->setText("Precio Total: "+ QString::number(s1PriceTotal));
                delete item;
             }

    }
    for(int row = 0; row < ui->s2->count(); row++)
    {
             QListWidgetItem *item = ui->s2->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                s2PriceTotal-=(item->text().split(" ")[item->text().split(" ").length()-1].toInt());
                ui->s2->removeItemWidget(item);
                ui->s2P->setText("Precio Total: "+ QString::number(s2PriceTotal));
                ui->s2->removeItemWidget(item);
                delete item;
             }

    }
    for(int row = 0; row < ui->s3->count(); row++)
    {
             QListWidgetItem *item = ui->s3->item(row);
             if (item->text().toLower().contains(deleteProducto)){
                s3PriceTotal-=(item->text().split(" ")[item->text().split(" ").length()-1].toInt());
                ui->s3->removeItemWidget(item);
                ui->s3P->setText("Precio Total: "+ QString::number(s3PriceTotal));
                ui->s3->removeItemWidget(item);
                delete item;
             }

    }


}


void Widget::get_TitleSupermarket(QString url_){
    QNetworkAccessManager* nam = new QNetworkAccessManager(this);
        connect(nam, &QNetworkAccessManager::finished, this, &Widget::setTitleSupermarket);
        QUrl url(url_);
        qDebug()<< "url: "<< url.toString(QUrl::FullyEncoded);
        nam->get(QNetworkRequest(url));
}
void Widget::setTitleSupermarket(QNetworkReply *reply){
    if(reply->error() == QNetworkReply::NoError){

        QByteArray result = reply->readAll();
        qDebug() << result;
        QJsonDocument jsonResponse = QJsonDocument::fromJson(result);

        QJsonObject obj = jsonResponse.object();
        QJsonArray array = obj["mercados"].toArray();

        for(const QJsonValue & value : array) {

            if (value["id"].toInt()==1){
                ui->labels1->setText(value["name_market"].toString());
                ui->labels1->setFont(QFont( "Arial", 10, QFont::Bold));
            }else if (value["id"].toInt()==2){
                ui->labels2->setText(value["name_market"].toString());
                ui->labels2->setFont(QFont( "Arial", 10, QFont::Bold));
            }else if(value["id"].toInt()==3){
                ui->labels3->setText(value["name_market"].toString());
                ui->labels3->setFont(QFont( "Arial", 10, QFont::Bold));
            }
        }
}
}


