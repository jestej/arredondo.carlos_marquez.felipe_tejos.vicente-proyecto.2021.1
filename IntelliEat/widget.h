#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QNetworkReply>
#include <QListWidgetItem>
QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();


private slots:
    void on_add_clicked();

    void on_delete_2_clicked();
    void chargerSupermarket(QNetworkReply *reply);
    void on_sDesear_itemDoubleClicked(QListWidgetItem *item);
    void get_TitleSupermarket(QString url_);
    void setTitleSupermarket(QNetworkReply *reply);


private:
    Ui::Widget *ui;
    int s1PriceTotal = 0;
    int s2PriceTotal = 0;
    int s3PriceTotal = 0;
};
#endif // WIDGET_H
