class Config:
    pass

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI='postgresql://postgres:2906@localhost/markets'
    SQLALCHEMY_TRACK_MODIFICATIONS= False
config = {
    'development' :DevelopmentConfig,
}