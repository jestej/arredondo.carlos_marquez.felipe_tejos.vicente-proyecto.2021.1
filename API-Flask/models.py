from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text,exc,func
from sqlalchemy.orm import backref,relationship
import sys

db = SQLAlchemy()

class Markets(db.Model):
    __tablename__='markets'
    id = db.Column(db.Integer, primary_key=True)
    name_market = db.Column(db.String(75), nullable=False)

    products= db.relationship('Products', cascade="all,delete", backref="markets")

    @classmethod
    def create(cls,_id,_name_market):
        _market= Markets(id=_id,name_market=_name_market)
        return _market.save()
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except exc.IntegrityError:
            print("Ooooops!", sys.exc_info(), "Ocurred.")
            return False
    def json(self):
        return {
            'id': self.id,
            'name_market': self.name_market
        }
    def update(self):
        self.save()
    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False
class Products(db.Model):
    __tablename__= 'products'
    idp = db.Column(db.Integer, primary_key=True)
    super_id = db.Column(db.Integer, db.ForeignKey('markets.id'))
    namep= db.Column(db.String(75), unique = False ,nullable =False)
    detallep= db.Column(db.String(140),nullable =False)
    marcap= db.Column(db.String(30),nullable =False)
    contienep= db.Column(db.String(43),nullable =False)
    slot= db.Column(db.Integer,nullable =False)
    price= db.Column(db.Integer,nullable =False)
    
    @classmethod
    def create(cls,_idp,_namep,_detallep,_slot,_price,_super_id,_marcap):
        _products= Products(idp=_idp,namep=_namep,detallep=_detallep,marcap=_marcap,slot=_slot,price=_price,super_id=_super_id)
        return _products.save()
    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return self
        except exc.IntegrityError:
            print("Oooops!",sys.exc_info(), "Ocurred.")
            return False
    def json(self):
        return{
            'idp': self.idp,
            'super_id':self.super_id,
            'nombrep': self.namep,
            'detallep': self.detallep,
            'marcap': self.marcap,
            'contienep': self.contienep,
            'slot': self.slot,
            'price': self.price
        }
    def update(self):
        self.save()
    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return True
        except:
            return False
