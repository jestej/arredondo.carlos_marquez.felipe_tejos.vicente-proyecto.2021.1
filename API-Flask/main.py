from flask import Flask, json
from flask import jsonify
from flask import request
from flask.helpers import flash, url_for
from flask.templating import render_template
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import redirect
from config import config
from models import db, Markets, Products

def create_app(enviroment):
    app = Flask(__name__)
    app.secret_key = "Secret Key"
    app.config.from_object(enviroment)
    with app.app_context():
        db.init_app(app)
        db.create_all()
    return app

enviroment = config['development']
app = create_app(enviroment)

# Pagina Inicio
@app.route('/')
def Home():
    return render_template("index.html")
# Pagina Mercados
@app.route('/mercados') 
def mercados():
    all_market = Markets.query.all()
    all_products = Products.query.all()
    return render_template("mercados.html", markets = all_market, products= all_products)

@app.route('/api/productos',methods=['GET'])
def getProductss():
    products = [ product.json() for product in Products.query.all() ]
    return jsonify({'productos': products})


# Mostrar informacion guardada de mercados
@app.route('/mercados',methods=['GET'])
def getMarkets():
    markets = [ market.json() for market in Markets.query.all() ]
    return jsonify({'mercados': markets})
# Mostrar informacion guardada de productos
def getProducts():
    products = [ product.json() for product in Products.query.all() ]
    return jsonify({'productos': products})

@app.route('/api/mercados',methods=['GET'])
def getMarkets1():
    markets = [ market.json() for market in Markets.query.all() ]
    return jsonify({'mercados': markets})

# Borrar data de mercados
@app.route('/mercados/delete/<id>',methods=['GET','POST'])
def deletemarket(id):
    my_market= Markets.query.get(id)
    db.session.delete(my_market)
    db.session.commit()
    flash("Delete Data!")
    return redirect(url_for('mercados'))

#Actualizar data de mercados
@app.route('/mercados/update',methods=['GET','POST'])
def updatemarket():
    if request.method == 'POST':
        my_market= Markets.query.get(request.form.get('id'))
        my_market.id= request.form['id']
        my_market.name_market = request.form['name_market']
        db.session.commit()
        flash("Update Data!")
        return redirect(url_for('mercados'))

#Actualizar data de productos
@app.route('/products/update',methods=['GET','POST'])
def updateproduct():
    if request.method == 'POST':
        my_product= Products.query.get(request.form.get('idp'))
        my_product.idp = request.form['idp']
        my_product.super_id = request.form['super_id']
        my_product.namep = request.form['namep']
        my_product.detallep = request.form['detallep']
        my_product.marcap = request.form['marcap']
        my_product.contienep= request.form['contienep']
        my_product.slot = request.form['slot']
        my_product.price = request.form['price']
        db.session.commit()
        flash("Update Data!")
        return redirect(url_for('mercados'))
    
#Borrar informacion de productos
@app.route('/products/delete/<idp>',methods=['GET','POST'])
def deletproduct(idp):
    my_product= Products.query.get(idp)
    db.session.delete(my_product)
    db.session.commit()
    flash("Delete Data!")
    return redirect(url_for('mercados'))
#Crear Supermercado
@app.route('/api/markets',methods=['POST'])
def crearMercado():
    if request.method == 'POST':
        idm = request.form['id']
        nameM = request.form['name_market']
        my_market = Markets(id=idm,name_market=nameM)
        db.session.add(my_market)
        db.session.commit()
        flash("Data Creada!")
        return redirect(url_for('mercados')) 
#Crear Productos
@app.route('/products',methods=['POST'])
def crearproducto():
    if request.method == 'POST':
        _idp = request.form['idp']
        _super_id = request.form['super_id']
        _namep= request.form['namep']
        _marcap = request.form['marcap']
        _detallep= request.form['detallep']
        _slot = request.form['slot']
        _contienep= request.form['contienep']
        _price= request.form['price']
        
        my_product = Products(idp=_idp,namep=_namep,detallep=_detallep,marcap=_marcap,contienep=_contienep,slot=_slot,price=_price,super_id=_super_id)
        db.session.add(my_product)
        db.session.commit()
        flash("Data Creada!")
        return redirect(url_for('mercados')) 



# Sujeto a cambios host
if __name__ == '__main__':
	app.run( host="192.168.0.6",  debug=True)